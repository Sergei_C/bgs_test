## Install on localhost

#### 1. Run docker-compose
> Install docker and docker-compose [here](https://www.docker.com/products/docker-desktop).
```bash
docker-compose up --build
```
> Stop docker - `ctrl + C`

#### 2. Wait until the packages are installed, migrations and seeders are complete
#### 3. Open API docs in your browser http://localhost/api.html
### `client_id` and `secret_id` are in the example request for `api/v1/login`. To work with other methods you need to add an authorization header with Bearer `token` received from `/api/v1/login`.
#### 4. Run tests
```bash
composer test
```

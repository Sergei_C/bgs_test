<?php

namespace App\Http\Middleware;

use App\Exceptions\Unauthorized;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param array $guards
     * @throws Unauthorized
     */
    protected function unauthenticated($request, array $guards)
    {
        throw new Unauthorized(__('api.access_error'));
    }
}

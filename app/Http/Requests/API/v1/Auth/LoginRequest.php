<?php

namespace App\Http\Requests\API\v1\Auth;

use App\Http\Requests\API\v1\MainRequest;

class LoginRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'bail|required|string|size:39|exists:clients',
            'secret_id' => 'bail|required|string|size:39',
        ];
    }
}

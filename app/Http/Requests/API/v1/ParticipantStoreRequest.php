<?php

namespace App\Http\Requests\API\v1;

class ParticipantStoreRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|string|between:2,255',
            'last_name' => 'bail|required|string|between:2,255',
            'email' => 'bail|required|string|email:rfc|unique:participants,email|between:5,255',
            'event_name' => 'bail|nullable|string|between:2,255|exists:events,name'
        ];
    }
}

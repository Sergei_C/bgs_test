<?php

namespace App\Http\Requests\API\v1;

class PaginationRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'per_page' => 'nullable|bail|integer|min:1',
            'page' => 'nullable|bail|integer|min:1',
        ];
    }
}

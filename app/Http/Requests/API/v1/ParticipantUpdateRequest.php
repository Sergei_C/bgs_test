<?php

namespace App\Http\Requests\API\v1;

use App\Rules\API\v1\UpdateEmail;

class ParticipantUpdateRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'bail|required_without_all:last_name,email,event_name|string|between:2,255',
            'last_name' => 'bail|sometimes|string|between:2,255',
            'email' => [
                'bail',
                'sometimes',
                'string',
                'email:rfc',
                'between:5,255',
                new UpdateEmail(preg_match('/^\d+$/', $this->id) ? $this->id : null)
            ],
            'event_name' => 'bail|sometimes|string|between:2,255|exists:events,name'
        ];
    }
}

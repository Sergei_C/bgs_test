<?php

namespace App\Http\Requests\API\v1;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantIndexRequest extends PaginationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                'event_name' => 'nullable|bail|string|regex:/^\w\D+$/|between:2,255'
            ]
        );
    }
}

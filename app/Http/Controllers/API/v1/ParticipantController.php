<?php

namespace App\Http\Controllers\API\v1;

use App\Events\ParticipantCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\v1\ParticipantIndexRequest;
use App\Http\Requests\API\v1\ParticipantStoreRequest;
use App\Http\Requests\API\v1\ParticipantUpdateRequest;
use App\Http\Resources\ParticipantCollection;
use App\Http\Resources\ParticipantResource;
use App\Models\Event;
use App\Models\Participant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * Class ParticipantController
 * @package App\Http\Controllers\API\v1
 */
class ParticipantController extends Controller
{
    /**
     * @param ParticipantIndexRequest $request
     * @return ParticipantCollection
     *
     * @api {get} /api/v1/participants Get list of all participants
     */
    public function index(ParticipantIndexRequest $request): ParticipantCollection
    {
        $participants = Participant::filterByEvents($request->get('event_name'));

        return new ParticipantCollection($participants->paginate($request->get('per_page')));
    }

    /**
     * @param ParticipantStoreRequest $request
     * @return JsonResponse|object
     *
     * @api {post} /api/v1/participants Create new participant
     */
    public function store(ParticipantStoreRequest $request): JsonResponse
    {
        $inputs = $request->validated();

        $participant = Participant::make($inputs);

        if (isset($inputs['event_name'])) {
            $participant->setEvent(Event::whereName($inputs['event_name'])->first());
        } else {
            $participant->setEvent(Event::all()->random());
        }

        $participant->save();

        event(new ParticipantCreated($participant));

        return (new ParticipantResource($participant))
            ->additional(['message' => __('api.success_create_participant')])
            ->response()
            ->setStatusCode(JsonResponse::HTTP_CREATED);
    }

    /**
     * @param $id
     * @return ParticipantResource
     *
     * @api {get} /api/v1/participants/:id Get participant by id
     */
    public function show($id): ParticipantResource
    {
        $participant = Participant::with('event')->findOrFail($id);
        return new ParticipantResource($participant);
    }

    /**
     * @param ParticipantUpdateRequest $request
     * @param $id
     * @return ParticipantResource
     *
     * @api {post} /api/v1/participants/:id Update participant by id
     */
    public function update(ParticipantUpdateRequest $request, $id)
    {
        $participant = Participant::findOrFail($id);

        $inputs = $request->validated();

        $participant->update($request->validated());

        if ($request->get('event_name')) {
            $participant->setEvent(Event::whereName($request->get('event_name'))->first());
            $participant->save();
        }

        return (new ParticipantResource($participant))
            ->additional(['message' => __('api.success_update_participant')]);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     *
     * @api {delete} /api/v1/participants/:id Delete user by id
     */
    public function delete($id)
    {
        Participant::findOrFail($id)->delete();

        return response()
            ->json(['message' => __('api.success_delete_participant')]);
    }
}

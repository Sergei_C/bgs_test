<?php

namespace App\Http\Controllers\API\v1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\v1\Auth\LoginRequest;
use App\Models\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return JsonResponse
     * @api {post} /api/v1/login Method for getting auth token
     */
    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['client_id' => $request->get('client_id'), 'password' => $request->get('secret_id')])) {
            /** @var Client $client */
            $client = Auth::user();
            $token = $client->createToken('client')->accessToken;

            return response()->json([
                'message' => __('api.success'),
                'data' => [
                    'token' => $token
                ]
            ]);
        } else {
            return response()->json(
                [
                    'message' => __('api.invalid_params'),
                    'errors' => [
                        'secret_id' => [
                            __('api.wrong_secret_id')
                        ]
                    ]
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}

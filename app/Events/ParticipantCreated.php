<?php

namespace App\Events;

use App\Models\Participant;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ParticipantCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var Participant */
    public $participant;

    /**
     * Create a new event instance.
     *
     * @param Participant $participant
     */
    public function __construct(Participant $participant)
    {
        $this->participant = $participant;
    }
}

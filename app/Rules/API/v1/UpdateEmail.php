<?php

namespace App\Rules\API\v1;

use App\Models\Participant;
use Illuminate\Contracts\Validation\Rule;

class UpdateEmail implements Rule
{
    /** @var int|null  */
    private $participantId;

    /**
     * Create a new rule instance.
     *
     * @param int|null $participantId
     */
    public function __construct(?int $participantId)
    {
        $this->participantId = $participantId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Participant::whereEmail($value)->where('id', '!=', $this->participantId)->first() == null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The email already exist.';
    }
}

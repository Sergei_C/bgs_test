<?php

namespace App\Listeners;

use App\Events\ParticipantCreated;
use App\Mail\Welcome as WelcomeMail;
use Illuminate\Support\Facades\Mail;

class SendParticipantCreated
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ParticipantCreated $event)
    {
        $message = new WelcomeMail($event->participant);
        Mail::to($event->participant->email)->send($message);
    }
}

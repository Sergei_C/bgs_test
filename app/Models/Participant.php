<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Participant
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property int $event_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Event $event
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Participant whereUpdatedAt($value)
 * @mixin Builder
 */
class Participant extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'event_id',
    ];

    /**
     * @inheritdoc
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @param string|null $eventName
     * @return Participant|Builder
     */
    public static function filterByEvents(string $eventName = null)
    {
        if ($eventName) {
            return self::with('event')->whereHas('event', function (Builder $query) use ($eventName) {
                $query->where('name', 'like', "%{$eventName}%");
            });
        } else {
            return self::with('event');
        }
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event)
    {
        $this->event_id = $event->id;
    }
}

<?php

namespace Tests\Unit;

use App\Models\Event;
use App\Models\Participant;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class ParticipantTest extends TestCase
{
    /**
     * Unit test for creating new participant.
     *
     * @return void
     */
    public function testCanCreateParticipant()
    {
        $data = factory(Participant::class)->make()->toArray();

        $data['event_name'] = Event::find($data['event_id'])->name;

        Queue::fake();
        $response = [
            'data' => [
                'id' => 1,
                'name' => $data['name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'event_name' => $data['event_name']
            ],
            'message' => __('api.success_create_participant')
        ];

        $this->post(route('createParticipant'), $data)
            ->assertStatus(201)
            ->assertJsonFragment($response);
    }

    /**
     * Unit test for getting participant by id.
     *
     * @return void
     */
    public function testCanShowParticipant()
    {
        $participant = factory(Participant::class)->create();

        $this->get(route('getParticipant', $participant->id))
            ->assertStatus(200);
    }

    /**
     * Unit test for updating participant by id.
     *
     * @return void
     */
    public function testCanUpdateParticipant()
    {
        $participant = factory(Participant::class)->create()->toArray();

        $data = [
            'name' => 'New name',
            'last_name' => 'New last name'
        ];

        $newData = array_merge($participant, $data);

        $newData['event_name'] = Event::find($newData['event_id'])->name;

        $response = [
            'data' => [
                'id' => $newData['id'],
                'name' => $newData['name'],
                'last_name' => $newData['last_name'],
                'email' => $newData['email'],
                'event_name' => $newData['event_name']
            ],
            'message' => 'Participant was successfully updated.'
        ];

        $this->post(route('updateParticipant', $participant['id']), $data)
            ->assertStatus(200)
            ->assertJsonFragment($response);
    }
}

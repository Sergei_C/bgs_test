<?php

return [
    'access_error' => 'Unauthorized.',
    'invalid_params' => 'The given data was invalid.',
    '404' => 'Participant not found.',
    'success_create_participant' => 'Participant was successfully created.',
    'success_update_participant' => 'Participant was successfully updated.',
    'success_delete_participant' => 'Participant was successfully deleted.',
    'wrong_secret_id' => 'Wrong secret id.',
    'success' => 'Success.',
];

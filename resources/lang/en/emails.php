<?php

return [
    'participant_created_subject' => 'New participant',
    'participant_created_body' => 'New participant :name was successfully created'
];

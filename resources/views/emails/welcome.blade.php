@component('mail::message')

@lang('emails.participant_created_body', ['name' => $name])

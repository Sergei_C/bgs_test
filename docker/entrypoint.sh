#!/bin/bash
set -e

# Composer install
cd /var/www && composer install --prefer-source --no-interaction

# If file .env not exists
if [ ! -f /var/www/.env ]; then
    # copy .env
    cd /var/www && cp .env.example .env
    # Key generate
    cd /var/www && php artisan key:generate
fi

php artisan migrate
php artisan passport:install --force
php artisan db:seed

service supervisor start
service cron start
service php7.3-fpm start
nginx -g 'daemon off;'

exec "$@"


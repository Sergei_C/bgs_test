<?php

use App\Models\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ClientSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Client::count()) {
            Client::create([
                'client_id' => config('auth.client_id'),
                'secret_id' => Hash::make(config('auth.secret_id'))
            ]);
        }
    }
}

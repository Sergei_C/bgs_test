<?php

use App\Models\Event;
use Illuminate\Database\Seeder;

class EventsSeederTabel extends Seeder
{
    private const EVENT_COUNT = 6;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Event::count()) {
            factory(Event::class, self::EVENT_COUNT)->create();
        }
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(config('app.events')),
        'date' => $faker->date(),
        'city' => $faker->city,
    ];
});

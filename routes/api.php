<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {
    Route::group(['prefix' => 'participants'], function () {
        Route::post('/', 'API\v1\ParticipantController@store')->name('createParticipant');
        Route::get('/', 'API\v1\ParticipantController@index')->name('getParticipantsList');
        Route::get('/{id}', 'API\v1\ParticipantController@show')->name('getParticipant');
        Route::post('/{id}', 'API\v1\ParticipantController@update')->name('updateParticipant');
        Route::delete('/{id}', 'API\v1\ParticipantController@delete')->name('deleteParticipant');
    });
});

Route::post('/login', 'API\v1\Auth\AuthController@login')->name('login');
